package org.beetl.sql.test;


import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.annotatoin.Sql;
import org.beetl.sql.core.engine.PageQuery;


public interface UserDao extends BaseDao<User> {
	
}
