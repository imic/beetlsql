package org.beetl.sql.core.mapping.type;

public interface PrimitiveValue {
	public Object getDefaultValue();
}
