package org.beetl.sql.core.orm;

public interface LazyEntity {
	public Object get();
	
}
